# Het Saldo - a De Lijn "smart"card saldo checker

This is an open source application to check the remaining trips on a [De Lijn](https://delijn.be) card ticket.
Since the transition from magstripe tickets to RFID based single/return/10trip/50trip/daypass tickets it was hard to
keep track of remaining trips of these cards, [pen and paper](https://blahaj.social/@maartje/111605883165867983) were the easiest way.

De Lijn has a [saldocheck page](https://www.delijn.be/en/tickets/saldochecker/) that requires you to re-enter the complex long ID every
time to check the information of the ticket which in practice is very unhandy for frequent users of public transport.

Using this API "Het Saldo" (a joke on De Lijn just meaning "the line") was built to store multiple tickets (as we all keep to never run out of 1.7 euro trips)
in a convenient cross-platform app.

This app stores all data locally and only reaches out to the De Lijn website to fetch the ticket data.

## Gimme!

You can get this app for:
* Web: [meyskens.github.io/het-saldo](https://meyskens.gitlab.io/het-saldo/)
* Android: [apk](https://gitlab.com/meyskens/het-saldo/-/jobs/artifacts/v1.0.0/raw/hetSaldo-1.0.0.apk?job=build%3Aapk) (soon on f-droid)
* iOS: [Testflight](https://testflight.apple.com/join/FZIDdWHz) (soon on App Store)
* Mobian/other Linux on Mobile: look in releases and/or CI runs for aarch64 Linux (and .deb) builds!

## Contribute!

All contributions to this project are very welcome! We use Flutter to build this app, feel free to submit a merge request to fix a bug, improve user experience or add a translation.

Not into code but into public transport? If you happen to have a "cursed ticket" on smartcard issued by De Lijn feel free to send it to [@maartje@blahaj.social](https://blahaj.social/@maartje) on the fediverse.
Currently, we lack any day tickets, 50 trip cards and some special tickets in our database of test cards.

## Special thanks to

* [The One With The Braid](https://gitlab.com/TheOneWithTheBraid/) for her amazing knowledge of Flutter to help me out with building this!

## Last notes

#### My bus stop was canceled why do I need this app?

This is NOT affiliated with De Lijn in ANY WAY. It was built by people fustrated by the state of public transport in Flanders as an attempt to improve things.

#### Hi I am from De Lijn can we copy this?

Only once you allow me to load 10 trip cards on MoBIB like MIVB/STIB and Le TEC (and stop forcing me to waste all these cards)!
