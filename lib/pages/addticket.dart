import 'package:flutter/material.dart';

import 'package:flutter_gen/gen_l10n/app_localizations.dart';

import 'package:hetsaldo/data/card.dart';
import 'package:hetsaldo/pages/utils/id_input_format.dart';
import 'package:hetsaldo/utils/ticket_manager.dart';

class AddTicket extends StatefulWidget {
  const AddTicket({super.key});

  @override
  State<AddTicket> createState() => _AddTicketState();
}

class _AddTicketState extends State<AddTicket> {
  TicketManager tickets = TicketManager.instance();

  _AddTicketState();

  final _ticketIDController = TextEditingController();
  bool _error = false;

  @override
  void initState() {
    super.initState();
  }

  void addTicket() async {
    try {
      setState(() {
        _error = false;
      });

      final ticketID = CardIdCleaner.cleanID(_ticketIDController.value.text);

      final ticket = SmartTicket(id: ticketID);
      await ticket.refreshData();

      if (ticket.isNotActive && context.mounted) {
        final acceptNotUsed = await showDialog<bool>(
          context: context,
          builder: (context) => AlertDialog(
            title: Text(AppLocalizations.of(context)!.notUsedNotice),
            actions: [
              TextButton(
                onPressed: () => Navigator.of(context).pop(false),
                child: Text(AppLocalizations.of(context)!.cancel),
              ),
              TextButton(
                onPressed: () => Navigator.of(context).pop(true),
                child: Text(AppLocalizations.of(context)!.notUsedAccept),
              ),
            ],
          ),
        );

        if (acceptNotUsed != true) throw Exception("Ticket not valid");
      }

      await tickets.add(ticket);

      if (mounted) Navigator.pop(context);
    } catch (e) {
      setState(() {
        ScaffoldMessenger.of(context)
            .showSnackBar(SnackBar(content: Text(e.toString())));
        _error = true;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(AppLocalizations.of(context)!.addCardTitle),
      ),
      body: ListView(
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: TextField(
              autofocus: true,
              autocorrect: false,
              maxLines: 1,
              controller: _ticketIDController,
              onSubmitted: (_) => addTicket(),
              inputFormatters: [IDInputFormatter()],
              decoration: InputDecoration(
                  border: const OutlineInputBorder(),
                  labelText: AppLocalizations.of(context)!.ticketNumber,
                  errorText: _error
                      ? AppLocalizations.of(context)!.pleaseAddValidTicketNumber
                      : null,
                  suffixIcon: IconButton(
                    icon: const Icon(Icons.arrow_circle_right),
                    tooltip: AppLocalizations.of(context)!.add,
                    onPressed: addTicket,
                  )),
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child:
                Text(AppLocalizations.of(context)!.insertTicketIDDescription),
          ),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Text(AppLocalizations.of(context)!.insertTicketIDNotice,
                style: const TextStyle(fontStyle: FontStyle.italic)),
          ),
          Image.asset(
            'assets/images/card.png',
            height: 250,
          ),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Text(
              AppLocalizations.of(context)!.onlyDeLijnNotice,
              textAlign: TextAlign.center,
              textScaler: const TextScaler.linear(0.9),
            ),
          ),
        ],
      ),
    );
  }
}
