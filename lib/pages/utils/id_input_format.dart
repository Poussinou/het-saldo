import 'package:flutter/services.dart';

// inspired by https://stackoverflow.com/questions/64386172/how-to-add-a-blank-space-after-every-4-characters-when-typing-in-textformfield
class IDInputFormatter extends TextInputFormatter {
  @override
  TextEditingValue formatEditUpdate(
      TextEditingValue oldValue, TextEditingValue newValue) {
    var text = newValue.text.replaceAll(' ', '').toUpperCase();

    if (newValue.selection.baseOffset == 0) {
      return newValue;
    }

    var buffer = StringBuffer();
    for (int i = 0; i < text.length; i++) {
      buffer.write(text[i]);
      var nonZeroIndex = i + 1;
      if (nonZeroIndex % 4 == 0 && nonZeroIndex != text.length) {
        buffer.write(' ');
      }
    }

    var selection = newValue.selection;
    // in case we add a space
    if (buffer.length == newValue.text.length + 1) {
      selection = TextSelection(
          baseOffset: selection.baseOffset + 1,
          extentOffset: selection.extentOffset + 1);
    }
    // in case we delete a space
    if (buffer.length < newValue.text.length) {
      selection = TextSelection(
          baseOffset: selection.baseOffset - 1,
          extentOffset: selection.extentOffset - 1);
    }

    var string = buffer.toString();
    return newValue.copyWith(text: string, selection: selection);
  }
}
