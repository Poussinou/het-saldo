import 'package:flutter/material.dart';

import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:hive_flutter/hive_flutter.dart';

import 'package:hetsaldo/pages/addticket.dart';
import 'package:hetsaldo/utils/ticket_manager.dart';
import 'package:hetsaldo/widgets/list_end.dart';
import 'package:hetsaldo/widgets/ticket.dart';

void main() async {
  await TicketManager.init();

  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Het Saldo',
      theme: ThemeData(
        colorScheme: ColorScheme.fromSeed(seedColor: const Color(0x00ffdd00)),
        useMaterial3: true,
      ),
      localizationsDelegates: const [
        GlobalMaterialLocalizations.delegate,
        GlobalWidgetsLocalizations.delegate,
        ...AppLocalizations.localizationsDelegates,
      ],
      supportedLocales: AppLocalizations.supportedLocales,
      home: const MyHomePage(),
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({super.key});

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  TicketManager tickets = TicketManager.instance();

  @override
  void initState() {
    super.initState();
    syncTickets();
  }

  Future<void> syncTickets() async {
    await tickets.syncAll();
    if (mounted) {
      ScaffoldMessenger.of(context).showSnackBar(SnackBar(
          content: Text(AppLocalizations.of(context)!.syncedAllTickets)));
    }
  }

  final GlobalKey<RefreshIndicatorState> _refreshKey =
      GlobalKey<RefreshIndicatorState>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        // TRY THIS: Try changing the color here to a specific color (to
        // Colors.amber, perhaps?) and trigger a hot reload to see the AppBar
        // change color while the oter colors stay the same.
        backgroundColor: Theme.of(context).colorScheme.inversePrimary,
        // Here we take the value from the MyHomePage object that was created by
        // the App.build method, and use it to set our appbar title.
        title: const Text("Het Saldo"),
        actions: [
          IconButton(
            icon: const Icon(Icons.refresh),
            tooltip: AppLocalizations.of(context)!.refreshData,
            onPressed: () => _refreshKey.currentState?.show(),
          ),
        ],
      ),
      body: Padding(
        padding: const EdgeInsets.only(bottom: 30),
        child: ValueListenableBuilder<Box>(
          valueListenable: tickets.box.listenable(),
          builder: (context, value, child) {
            if (tickets.box.length < 1) {
              return Center(
                  child: Text(AppLocalizations.of(context)!.pleaseAddATicket));
            }
            return RefreshIndicator(
              key: _refreshKey,
              onRefresh: syncTickets,
              child: ListView.builder(
                  itemCount: tickets.box.length + 1,
                  itemBuilder: (context, index) {
                    if (index == tickets.box.length) {
                      return const Padding(
                        padding: EdgeInsets.only(bottom: 20),
                        child: ListEnd(),
                      );
                    }

                    final ticket = tickets.box.getAt(index);
                    if (ticket == null) {
                      return Text(
                          AppLocalizations.of(context)!.errorLoadingTicket);
                    }
                    return Padding(
                      padding: const EdgeInsets.only(top: 1, bottom: 8),
                      child: TicketWidget(ticket: ticket),
                    );
                  }),
            );
          },
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          Navigator.push(context,
              MaterialPageRoute(builder: (context) => const AddTicket()));
        },
        tooltip: AppLocalizations.of(context)!.addCard,
        child: const Icon(Icons.add),
      ), // This trailing comma makes auto-formatting nicer for build methods.
    );
  }
}
