abstract class Version {
  const Version._();

  static const isStable = bool.fromEnvironment(
    'HETSALDO_IS_STABLE',
    defaultValue: false,
  );

  static const version = String.fromEnvironment(
    'HETSALDO_VERSION',
    defaultValue: 'debug',
  );

  static const gitlabRepoBase = 'https://gitlab.com/meyskens/het-saldo';
}
