import 'package:flutter/foundation.dart';

import 'package:hive_flutter/hive_flutter.dart';

import '../data/card.dart';

const ticketsBox = 'tickets';

class TicketManager extends ValueListenable<TicketManager> {
  static TicketManager? _instance;

  final Box<SmartTicket> box = Hive.box<SmartTicket>(ticketsBox);

  factory TicketManager.instance() {
    assert(
      _instance != null,
      'TicketManager was not initialized yet. Please await TicketManager.init first.',
    );
    return _instance!;
  }

  TicketManager();

  static Future<void> init() async {
    Hive.registerAdapter(SmartTicketAdapter());
    Hive.registerAdapter(SmartTicketLineAdapter());

    await Hive.initFlutter();
    await Hive.openBox<SmartTicket>(ticketsBox);

    _instance = TicketManager();
  }

  Future<void> syncAll() async {
    for (var i = 0; i < box.length; i++) {
      final t = box.getAt(i);
      if (t == null) continue;
      await t.refreshData();
      await box.putAt(i, t);
    }
  }

  Future<void> add(SmartTicket t) async {
    for (var i = 0; i < box.length; i++) {
      final dbt = box.getAt(i);
      if (t.id == dbt!.id) throw Exception('Ticket already exists');
    }
    await box.add(t);
  }

  Future<void> delete(SmartTicket t) async {
    for (var i = 0; i < box.length; i++) {
      final dbt = box.getAt(i);
      if (t.id != dbt!.id) continue;
      await box.deleteAt(i);
      return;
    }
  }

  Future<void> sync(SmartTicket t) async {
    for (var i = 0; i < box.length; i++) {
      final dbt = box.getAt(i);
      if (t.id != dbt!.id) continue;
      await t.refreshData();
      box.putAt(i, t);
      return;
    }
  }

  @override
  void addListener(VoidCallback listener) =>
      box.listenable().addListener(listener);

  @override
  void removeListener(VoidCallback listener) =>
      box.listenable().removeListener(listener);

  @override
  TicketManager get value => this;
}
