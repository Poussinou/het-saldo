import 'package:flutter/material.dart';

import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:url_launcher/link.dart';

import '../utils/version.dart';

class ListEnd extends StatelessWidget {
  const ListEnd({super.key});

  void showInfoDialog(BuildContext context) => showAboutDialog(
        context: context,
        applicationVersion: Version.version,
        applicationIcon: Image.asset(
          'assets/logo/logo.png',
          width: 64,
          height: 64,
        ),
        applicationLegalese: AppLocalizations.of(context)!.appDescription,
        children: [
          Link(
            uri: Uri.parse(Version.gitlabRepoBase),
            builder: (context, followLink) {
              return OutlinedButton.icon(
                onPressed: followLink,
                icon: const Icon(Icons.code),
                label: const Text("Source Code"),
              );
            },
          ),
          // looking for more inspiration here?
        ]
            .map(
              (e) => Padding(
                padding: const EdgeInsets.symmetric(vertical: 4),
                child: e,
              ),
            )
            .toList(),
      );

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Center(
          child: Text(
            AppLocalizations.of(context)!.notice24HourDelay,
            textAlign: TextAlign.center,
            textScaler: const TextScaler.linear(1.2),
          ),
        ),
        Padding(
          padding: const EdgeInsets.only(top: 4),
          child: Center(
            child: InkWell(
              onTap: () => showInfoDialog(context),
              child: const Text(
                'Het Saldo - version ${Version.version}',
                textAlign: TextAlign.center,
                textScaler: TextScaler.linear(0.9),
                style: TextStyle(
                  decoration: TextDecoration.underline,
                ),
              ),
            ),
          ),
        ),
      ],
    );
  }
}
