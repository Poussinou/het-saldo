// ignore_for_file: use_build_context_synchronously

import 'package:flutter/material.dart';

import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:intl/intl.dart';
import 'package:ionicons/ionicons.dart';

import 'package:hetsaldo/data/card.dart';
import 'package:hetsaldo/utils/ticket_manager.dart';

class TicketDetails extends StatelessWidget {
  final SmartTicket ticket;
  final TicketManager manager = TicketManager.instance();

  final dateFormat = DateFormat('yyyy-MM-dd kk:mm');

  TicketDetails({super.key, required this.ticket});

  Future<void> delete(BuildContext context) async {
    final delete = await showDialog<bool>(
      context: context,
      builder: (context) => AlertDialog(
        title: Text(
            AppLocalizations.of(context)!.sureDeleteTicket(ticket.prettyID())),
        actions: [
          TextButton(
            onPressed: () => Navigator.of(context).pop(false),
            child: Text(AppLocalizations.of(context)!.cancel),
          ),
          TextButton(
            onPressed: () => Navigator.of(context).pop(true),
            child: Text(AppLocalizations.of(context)!.delete),
          ),
        ],
      ),
    );
    if (delete != true) return;
    await manager.delete(ticket);
  }

  @override
  Widget build(BuildContext context) {
    return Wrap(
      children: [
        TicketUses(ticket: ticket),
        Row(
          mainAxisAlignment: MainAxisAlignment.end,
          children: <Widget>[
            IconButton(
              icon: const Icon(Icons.refresh),
              onPressed: () async {
                await manager.sync(ticket);
                ScaffoldMessenger.of(context).showSnackBar(SnackBar(
                    content: Text(AppLocalizations.of(context)!.syncedTicket)));
              },
            ),
            const SizedBox(width: 8),
            IconButton(
              icon: const Icon(Icons.delete),
              onPressed: () => delete(context),
            ),
            const SizedBox(width: 8),
          ],
        ),
      ],
    );
  }
}

class TicketUses extends StatelessWidget {
  final SmartTicket ticket;

  final dateFormat = DateFormat('yyyy-MM-dd kk:mm');

  TicketUses({super.key, required this.ticket});

  @override
  Widget build(BuildContext context) {
    if (ticket.smartTicketLines == null || ticket.smartTicketLines!.isEmpty) {
      return Text(AppLocalizations.of(context)!.ticketNeverUsed);
    }

    final uses = ticket.smartTicketLines!;

    return Wrap(
      children: List.generate(
        uses.length,
        (index) {
          final useDate =
              DateTime.fromMillisecondsSinceEpoch(uses[index].timestamp!);
          return Row(
            children: [
              Flexible(
                child: Container(
                  decoration: const BoxDecoration(
                    border: Border(
                      top: BorderSide(
                        color: Colors.grey,
                        width: 1.3,
                      ),
                    ),
                  ),
                  child: ListTile(
                    leading: const Icon(Ionicons.bus_outline),
                    title: Text(dateFormat.format(useDate)),
                    subtitle: Text(AppLocalizations.of(context)!
                        .tripsLeft(uses[index].value!)),
                  ),
                ),
              ),
            ],
          );
        },
      ),
    );
  }
}
