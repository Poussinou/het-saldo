Het Saldo is a De Lijn "smart"card saldo checker.

It allows you to easily keep track of the remaining trips on your De Lijn tickets. The app lets you add multiple tickets and syncs their status and remaining balance for you to check.